# Jannes & Mannes OpenCart category module third level children

This VQmod OpenCart extension enables third level children to your category module. Be sure to also add some extra code to your template.

	:::php
		if (!empty($child['children'])) :
			echo '<ul>';
				foreach ($child['children'] as $category_level3) :
					if ($category_level3['category_id'] == $child2_id) :
						echo '<li><a href="'.$category_level3['href'].'" class="active"> - '.$category_level3['name'].'</a></li>';
					else :
						echo '<li><a href="'.$category_level3['href'].'"> - '.$category_level3['name'].'</a></li>';
					endif;
				endforeach;
			echo '</ul>';
		endif;